# helpdesk


##################ACESSO_AO_SISTEMA#################################

Para acessar via tela e nevegar logue com o usuário a seguir

usuário: admin@helpdesk.com

senha: 123456


As telas são acessedas de acordo com cada Perfil.

Exemplo:

ROLE_ADMIN -> Terá perfil de criar usuários ver o summary

ROLE_TECNICIAN -> Terá acesso a criar um ticket, associar ticket, consultar, etc.

ROLE_CUSTOMER -> Terá acesso a criar um ticket, associar ticket, consultar, etc.

OU seja para uma boa navegada no sistema sugiro que crie os outros dois perfies, tecnician e customer, lembrando que apenas o administrador 
poderá criar um usuário.


Neste projeto há duas estrutura de pasta uma contenco o projeto cliente
e a outra o service.

o client é baseado no framework angular 7 e o service com spring, spring security e database H2.


Abaixo segue algumas configurações.

#URL do Swagger para visualização dos end-points
http://localhost:8080/swagger-ui.html#

#URL do banco de dados
http://localhost:8080/h2/

dados para acesso

JDBC URL: jdbc:h2:mem:teste_alelo

User Name: sa

Password:

######################CLEINT########################

# Helpdesk


Para rodar o projeto cliente será necessário cloná-lo.

após o clone afetuar o seguinte comando 

npm install na pasta client do projeto.

após finalizar a instalação das dependências

suba o contexto com 

ng serve

Obs.: É imprescindível a instalação do nodejs 10, npm e angularcli versão 7


######################SERVICE########################


Para rodar o projeto service será necessário cloná-lo.

após o clone afetuar o seguinte comando 

mvn clean install na pasta service do projeto.

após finalizar a instalação das dependências

suba o contexto com 

mvn spring-boot:run 
ou
java -jar "nome_do_pacote"

Obs.: É imprescindível a instalação do java 8, maven.

