package com.helpdeskservice.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.helpdeskservice.entities.ChangesStatus;

@Repository
public interface ChangesStatusRepository extends JpaRepository<ChangesStatus, Long>{
	
	Iterable<ChangesStatus> findByTicketIdOrderByDateChangeStatusDesc(Long ticketId);

}
