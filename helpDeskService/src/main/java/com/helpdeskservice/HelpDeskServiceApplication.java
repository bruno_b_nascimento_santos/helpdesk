package com.helpdeskservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.helpdeskservice.entities.Endereco;
import com.helpdeskservice.entities.User;
import com.helpdeskservice.entities.enumerators.Profile;
import com.helpdeskservice.repositories.EnderecoRepository;
import com.helpdeskservice.repositories.UserRepository;

@SpringBootApplication
public class HelpDeskServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelpDeskServiceApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(UserRepository userRepository,EnderecoRepository enderecoRepository, PasswordEncoder passwordEncoder) {
		return args ->{
			initUsers(userRepository,enderecoRepository, passwordEncoder);
		};
	}
	
	private void initUsers(UserRepository userRepository, EnderecoRepository enderecoRepository,PasswordEncoder passwordEncoder) {
		User admin = new User();
		admin.setEmail("admin@helpdesk.com");
		admin.setPassword(passwordEncoder.encode("123456"));
		admin.setProfile(Profile.ROLE_ADMIN);
		Endereco endereco = new Endereco();
		User find = userRepository.findByEmail(admin.getEmail());
		if(find == null) {
			userRepository.save(admin);
			endereco.setUser(admin);
			endereco.setBairro("teste");
			endereco.setCep("00000-090");
			endereco.setComplemento("casa");
			endereco.setLocalidade("SP");
			endereco.setLogradouro("teste");
			endereco.setNumero("S/N");
			endereco.setUf("SP");
			enderecoRepository.save(endereco);
		}
	}

}

