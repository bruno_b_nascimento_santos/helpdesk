package com.helpdeskservice.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.helpdeskservice.entities.User;

public interface UserService {
	
	User findByEmail(String email);
	
	User createdOrUpdate(User user);
	
	User findById(Long id);
	
	void delete(Long id);
	
	Page<User> findAll(Pageable pageable);

}
