package com.helpdeskservice.service.viaCep;

import com.helpdeskservice.service.viaCep.response.CepEnderecoResponse;

public interface CepService {
	CepEnderecoResponse getCep(String buscarCep);
}
