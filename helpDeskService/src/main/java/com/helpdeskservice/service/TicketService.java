package com.helpdeskservice.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.helpdeskservice.entities.ChangesStatus;
import com.helpdeskservice.entities.Ticket;

@Component
public interface TicketService {
		
	Ticket createOrUpdate(Ticket ticket, HttpServletRequest request);
	
	Ticket findById(Long id);
	
	void delete(Long id);
	
	Page<Ticket> listTicket(int page, int count);
	
	ChangesStatus createChangeStatus(ChangesStatus changesStatus);
	
	Iterable<ChangesStatus> listChangeStatus(Long ticketId);
	
	Page<Ticket> findByCurrentUser(int page, int count, Long userId);
	
	Page<Ticket> findByParameters(int page, int count, String title, String status, String priority);
	
	Page<Ticket> findByParametersCurrentUser(int page, int count, String title, String status, String priority, Long userId);
	
	Page<Ticket> findByNumber(int page, int count, Integer number);
	
	Iterable<Ticket> findAll();
	
	Page<Ticket> findByParameterAndAssignedUser(int page, int count, String title, String status, String priority, Long assignedUser);

	Ticket createOrUpdateTicketWithIdAndStatus(Ticket ticket, Long id, String status, HttpServletRequest request);
	 
}
