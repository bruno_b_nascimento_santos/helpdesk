package com.helpdeskservice.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.helpdeskservice.entities.Endereco;
import com.helpdeskservice.entities.User;
import com.helpdeskservice.repositories.EnderecoRepository;
import com.helpdeskservice.repositories.UserRepository;
import com.helpdeskservice.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Override
	public User findByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

	@Override
	public User createdOrUpdate(User user) {
		user = this.userRepository.save(user);
		enderecoRepository.save(user.getEndereco());
		return user;
	}

	@Override
	public User findById(Long id) {
		User user = this.userRepository.findOne(id);
		enderecoRepository.findByUserId(user.getId());
		return user;
	}

	@Override
	public void delete(Long id) {
		this.userRepository.delete(id);		
	}

	@Override
	public Page<User> findAll(Pageable pageable) {
		Page<User> users = this.userRepository.findAll(pageable);
		//List<User> list = users.getContent().stream().map(u -> getEndereco(u)).collect(Collectors.toList());
		for(User user: users.getContent()) {
			user.setEndereco(getEndereco(user));
		}
		return users;
	}

	private Endereco getEndereco(User user) {		
		Endereco endereco = enderecoRepository.findByUserId(user.getId());		
		return endereco;
	}

}
