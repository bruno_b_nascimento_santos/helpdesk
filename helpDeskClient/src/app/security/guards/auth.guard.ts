import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharedService } from '../../shared/service/impl/shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate , CanLoad{

  public shared: SharedService;

  constructor(private router: Router){
    this.shared = SharedService.getInstance();
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.verificaAcesso();
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean{
    return this.verificaAcesso();
  }

  private verificaAcesso(): boolean {
    if(this.shared.isLoggedIn()){
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }


}
