import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { TicketService } from '../ticket-service-interface';
import { Ticket } from '../../model/ticket.model';
import { HELP_DESK_API } from '../../config/help-desk-api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketServiceImpl implements TicketService {

  private TICKETAPI = '/api/ticket';

  constructor(private http: HttpClient) {

  }

  createOrUpdate(ticket: Ticket):Observable<any>{
    if(ticket.id != null || ticket.id != ''){
      return this.http.post(`${HELP_DESK_API}${this.TICKETAPI}`, ticket);
    }else{
      return this.http.put(`${HELP_DESK_API}${this.TICKETAPI}`, ticket);
    }
  }

  findAll(page: number, count: number):Observable<any>{
    return this.http.get(`${HELP_DESK_API}${this.TICKETAPI}/${page}/${count}`);
  }

  findById(id: string):Observable<any>{
    return this.http.get(`${HELP_DESK_API}${this.TICKETAPI}/${id}`);
  }

  delete(id: string):Observable<any>{
    return this.http.delete(`${HELP_DESK_API}${this.TICKETAPI}/${id}`);
  }

  findParameter(page: number, count: number, assigned: boolean, t: Ticket):Observable<any>{
    t.number = t.number == null ? 0 : t.number;
    t.title = t.title == '' ? 'uninformed' : t.title;
    t.status = t.status == '' ? 'uninformed' : t.status;
    t.priority = t.priority == '' ? 'uninformed' : t.priority;
    return this.http.get(`${HELP_DESK_API}${this.TICKETAPI}/${page}/${count}/${t.number}/${t.title}/${t.status}/${t.priority}/${assigned}`);
  }

  changeStatus(status: string, ticket: Ticket):Observable<any>{
    return this.http.put(`${HELP_DESK_API}${this.TICKETAPI}/${ticket.id}/${status}`, ticket);
  }



}
