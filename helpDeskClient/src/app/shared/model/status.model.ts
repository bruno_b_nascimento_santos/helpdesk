export class Status {

  constructor(public id: number, public descriptionView: string, public descriptionBack: string){}
}
