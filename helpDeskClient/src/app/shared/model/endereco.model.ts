export class Endereco {

    constructor(public id: string, public logradouro: string, public numero: string, public complemento: string
        , public bairro: string, public cep: string, public localidade: string,  public uf: string){
  
    }
  
  }